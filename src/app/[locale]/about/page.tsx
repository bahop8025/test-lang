import { useTranslations } from "next-intl";
import React from "react";

export default function About() {
  const t = useTranslations("About");
  return (
    <div>
      <h1>{t("message")}</h1>
    </div>
  );
}
